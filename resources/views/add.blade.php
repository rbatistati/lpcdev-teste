@extends('layouts.app')

@section('content')
    <div id="content-container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet">
                            <div class="portlet-header">
                                <h3>
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                    Clientes Cadastrados
                                </h3>
                                <ul class="portlet-tools pull-right">
                                    <li>
                                        <button id="addForma" class="button-menu-mobile open-left" data-toggle="modal">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </button>
                                    </li>
                                    <li>
                                        <button id="editarForma" data-events="actionEvents"
                                                class="button-menu-mobile open-left" data-toggle="modal">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>
                                    </li>
                                    <li>
                                        <button id="apagarForma" class="button-menu-mobile open-left"
                                                data-toggle="modal">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.portlet-header -->
                            <div class="portlet-content">

                                <table data-page-size="10" id="tblFormas" data-locale="pt-BR"
                                       class="table table-striped"
                                       data-toggle="table" data-classes="table table-no-bordered"
                                       data-url="{{ url('/client/listClientes/') }}" data-side-pagination="server"
                                       data-page-list="[5, 10, 20, 50, 100, 200]"
                                       data-pagination="true" data-filter-control="true" data-filter-show-clear="true">
                                    <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true"></th>
                                        <th data-visible="false" data-field="id"></th>
                                        <th data-field="name">Nome</th>
                                        <th data-field="numero_telefone">Telefone</th>
                                        <th data-field="email" data-filter-control="input">E-mail</th>
                                        <th data-field="tipo_cliente" data-filter-control="select">Tipo Cliente</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.portlet-content -->
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-md-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
@endsection
@push('scripts')
{{--JS Page--}}
<script src="{{ asset('js/add/add.js') }}"></script>
<script type="text/javascript">
    $("#addForma").click(function(){
        $('#modal').load('{{URL::to("client/edit")}}', function() {
            $('#modalAddForma').modal();
        });
    });
</script>
@endpush

