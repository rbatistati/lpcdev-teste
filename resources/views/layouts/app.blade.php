<!-- Stored in resources/views/layouts/app.blade.php -->

<html>
<head>
    <title>App Name - @yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{--BootstrapTable--}}
    <link href="{{ asset('css/bootstrap-table.css') }}" rel="stylesheet">
    {{--SweetAlert--}}
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    {{--BootstrapSelect--}}
    <link href="{{ asset('css/bootstrap-select.css') }}" rel="stylesheet">
    {{--Validador--}}
    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>
<body>
@section('sidebar')
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/client/index/') }}">Clientes</a></li>
                            <li><a href="{{ url('/client/omie/') }}">Clientes OMIE</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
@show

<div class="container">
    @yield('content')
</div>
<div id="modal"></div>
{{--Jquery--}}
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
{{--Bootstrap--}}
<script src="{{ asset('js/app.js') }}"></script>
{{--BootstrapTable--}}
<script src="{{ asset('js/bootstrap-table.js') }}"></script>
<script src="{{ asset('js/bootstrap-table-pt-BR.js') }}"></script>
<script src="{{ asset('js/bootstrap-table-toolbar.js') }}"></script>
<script src="{{ asset('js/bootstrap-table-filter-control.js') }}"></script>
{{--SweetAlert--}}
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
{{--Notify--}}
<script src="{{ asset('js/bootstrap-notify.js') }}"></script>
{{--BootstrapSelect--}}
<script src="{{ asset('js/bootstrap-select.js') }}"></script>
{{--Validador--}}
<script src="{{ asset('js/parsley.js') }}"></script>
<script src="{{ asset('js/pt-br.js') }}"></script>
{{--AjaxForm--}}
<script src="{{ asset('js/jquery.form.js') }}"></script>

@stack('scripts')
</body>
</html>