@extends('layouts.app')

@section('content')
    <div id="content-container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet">
                            <div class="portlet-header">
                                <h3>
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                    Clientes Cadastrados - OMIE
                                </h3>
                            </div>
                            <!-- /.portlet-header -->
                            <div class="portlet-content">

                                <table data-page-size="50" id="tblFormas" data-locale="pt-BR"
                                       class="table table-striped"
                                       data-toggle="table" data-classes="table table-no-bordered"
                                       data-url="{{ url('/client/listClientesOmie/') }}" data-side-pagination="server"
                                       data-page-list="[5, 10, 20, 50, 100, 200]"
                                       data-pagination="true">
                                    <thead>
                                    <tr>
                                        <th data-field="id">Código</th>
                                        <th data-field="name">Nome Fantasia</th>
                                        <th data-field="email">E-mail</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.portlet-content -->
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-md-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#tblFormas');
        table.bootstrapTable('showLoading');
    });
</script>
@endpush
