<?php

if(empty($oCliente)){
    $oCliente = new stdClass();

} else {
    $oCliente = $oCliente[0];
}
$sTipo = isset($oCliente->tipo_cliente) ? $oCliente->tipo_cliente : null;
?>
<div class="modal fade" id="modalAddForma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Criar/Editar Cliente</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmformapagamento" name="frmformapagamento" method="post" data-parsley-validate action="{{ url('/client/save/') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id='id' value="{{ $oCliente->id or NULL }}"/>
                    <div class="row">
                        <div class="col-sm-10">
                            <label>Nome: </label>
                            <input type="text" name="name" parsley-trigger="change" required placeholder="Nome" class="form-control" value="{{ $oCliente->name or NULL }}" id="name"/>
                        </div>
                        <div class="col-sm-5">
                            <label>Email:</label>
                            <input type="text" name="email"  placeholder="E-Mail" data-parsley-trigger="change" required data-parsley-type="email" class="form-control" value="{{ $oCliente->email or NULL }}" id="email"/>
                        </div>
                        <div class="col-sm-5">
                            <label for="tipo_cliente">Tipo Cliente:</label>
                            <select required class="form-control selectpicker show-tick" name="tipo_cliente" id="tipo_cliente">
                                <option {{ $sTipo == \App\Http\Controllers\ClientController::TIPO_CLIENTE_PESSOA_FISICA ? "selected" : "" }} value='{{ \App\Http\Controllers\ClientController::TIPO_CLIENTE_PESSOA_FISICA }}'>Física</option>
                                <option {{ $sTipo == \App\Http\Controllers\ClientController::TIPO_CLIENTE_PESSOA_JURIDICA ? "selected" : "" }} value='{{ \App\Http\Controllers\ClientController::TIPO_CLIENTE_PESSOA_JURIDICA }}'>Jurídica</option>
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <label>Telefone:</label>
                            <input type="text" name="numero_telefone"  placeholder="Número de Telefone" data-parsley-trigger="change" required data-parsley-type="digits" class="form-control" value="{{ $oCliente->numero_telefone or NULL }}" id="numero_telefone"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="closesalvarformapagamento" class="btn btn-default" data-dismiss="modal">Fechar
                </button>
                <button type="button" onclick="javascript:$('#frmformapagamento').submit()" id='salvarformapagamento' data-loading-text="Enviando..." class="btn btn-primary">
                    Salvar
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('.selectpicker').selectpicker({
            language: 'pt-BR'
        });
    });

    $('#frmformapagamento').parsley();

    $("#frmformapagamento").ajaxForm({
        dataType: 'json',
        success: function (data) {
            swal("", data.msg, data.class);
            if (data.class != 'error') {
                $("#modalAddForma").modal('hide');
                $("#tblFormas").bootstrapTable('refresh');
            }
        }
    });
</script>