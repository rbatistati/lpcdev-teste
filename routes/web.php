<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('welcome');
});

Route::get('client/index', 'ClientController@index')->name('add');
Route::get('client/omie', 'ClientController@omie')->name('omie');
Route::post('client/save', 'ClientController@save')->name('save');
Route::get('client/listClientes', 'ClientController@listClientes')->name('listClientes');
Route::get('client/listClientesOmie', 'ClientController@listClientesOmie')->name('listClientesOmie');
Route::get('client/edit', 'ClientController@edit')->name('edit');
Route::post('client/edit', 'ClientController@edit')->name('edit');
Route::post('client/delete', 'ClientController@delete')->name('delete');