FROM php:5.6-apache

MAINTAINER Luiz Paulo Camargo <luizpcam@lpcdev.com.br>

RUN apt-get update && apt-get install -y wget libssl-dev libfreetype6-dev libjpeg62-turbo-dev \
        libmcrypt-dev libpng12-dev git vim curl \
    && docker-php-ext-install -j$(nproc) iconv mcrypt zip

RUN docker-php-ext-install pdo_mysql

RUN apt-get update && \
    apt-get install -y libxml2-dev && \
    docker-php-ext-install soap

RUN pecl install mongodb && docker-php-ext-enable mongodb

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN a2enmod rewrite
RUN a2enmod headers

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

