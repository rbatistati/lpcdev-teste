<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use Respect\Validation\Validator;
use App\Entities\Clientes;
use App\Repository\ClientesRepo;

class ClientController extends Controller
{

    CONST TIPO_CLIENTE_PESSOA_FISICA   = 'F';
    CONST TIPO_CLIENTE_PESSOA_JURIDICA = 'J';

    private $repo;

    public function __construct(ClientesRepo $repo)
    {
        $this->repo = $repo;
    }

    public function index() {
        return view('add');
    }

    public function omie() {
        return view('omie');
    }

    public function save(Request $request) {

        $oRetorno = new \stdClass();
        $oRetorno->class = 'success';
        $oRetorno->msg   = 'Feito';

        if(!Validator::email()->validate($request->get('email'))){
            $oRetorno->class = 'error';
            $oRetorno->msg   = 'Email inválido';
            return json_encode($oRetorno);
        }
        if(!Validator::phone()->validate($request->get('numero_telefone'))){
            $oRetorno->class = 'error';
            $oRetorno->msg   = 'Número inválido inválido';
            return json_encode($oRetorno);
        }
        if(!Validator::in(array(self::TIPO_CLIENTE_PESSOA_FISICA,self::TIPO_CLIENTE_PESSOA_JURIDICA))->validate($request->get('tipo_cliente'))){
            $oRetorno->class = 'error';
            $oRetorno->msg   = 'Tipo Pessoa inválido';
            return json_encode($oRetorno);
        }

        if($request->get('id') == null) {

            $this->repo->create($this->repo->prepareData($request));

        } else {

            $oCliente = $this->repo->CleinteById($request->get('id'));

            try{

                $this->repo->update($oCliente,$request);

            } catch (\Exception $ex){

                $oRetorno->class = 'error';
                $oRetorno->msg   = 'Erro ao realizar operação. Detalhes:'.$ex->getMessage();

            }
        }

        return json_encode($oRetorno);
    }

    /**
     * Método que retorna todos os clientes cadastrados
     */
    public function listClientes(Request $request){
        $aRetorno = array();
        if($request->get('filter') != NULL){
            $oFilter = json_decode($request->get('filter'));
            if(isset($oFilter->email) && !isset($oFilter->tipo_cliente)) {
                $oClientes = Cliente::where('email', 'like', '%' . $oFilter->email . '%')->get();
            }elseif(isset($oFilter->tipo_cliente) && !isset($oFilter->email)) {
                $sTipo = $oFilter->tipo_cliente == 'Fisica' ? self::TIPO_CLIENTE_PESSOA_FISICA : self::TIPO_CLIENTE_PESSOA_JURIDICA;
                $oClientes = Cliente::where('tipo_cliente', 'like', '%' . $sTipo . '%')->get();
            }elseif(isset($oFilter->tipo_cliente) && isset($oFilter->email)){
                $sTipo = $oFilter->tipo_cliente == 'Fisica' ? self::TIPO_CLIENTE_PESSOA_FISICA : self::TIPO_CLIENTE_PESSOA_JURIDICA;
                $oClientes = Cliente::where('tipo_cliente', 'like', '%' . $sTipo . '%')->where('email', 'like', '%' . $oFilter->email . '%')->get();
            }
        } else {
            $oClientes = Cliente::all();
        }
        $aRetorno['total'] = $oClientes->count();

        foreach ($oClientes->toArray() as $oCliente){

            $oDados = new \stdClass();
            $oDados->id              = $oCliente['id'];
            $oDados->name            = $oCliente['name'];
            $oDados->numero_telefone = $oCliente['numero_telefone'];
            $oDados->email           = $oCliente['email'];
            $oDados->tipo_cliente    = $oCliente['tipo_cliente'] == self::TIPO_CLIENTE_PESSOA_FISICA ? 'Fisica' : 'Jurdica';
            $aRetorno['rows'][] = $oDados;
        }

        return $aRetorno;
    }

    public function edit(Request $request){
        $oCliente = NULL;
        if(!empty($request->get('id'))){
            $oCliente = Cliente::where('id', $request->get('id'))->get();

        }
        return view('modaladd',compact('oCliente'));
    }

    public function delete(Request $request){
        $oRetorno = new \stdClass();
        $oRetorno->class = 'success';
        $oRetorno->msg   = 'Feito';

        foreach($request->get('dados') as $id) {

            $oCliente = Cliente::find($id);

            try {
                $oCliente->delete();
            } catch (\Exception $ex) {
                $oRetorno->class = 'error';
                $oRetorno->msg = 'Operação não realizada. Detalhes:'.$ex->getMessage();
                break;
            }
        }
        return json_encode($oRetorno);
    }

    public function listClientesOmie(Request $request){
        $aRetorno = array();
        $payload = ['pagina' => ($request->get('offset')/$request->get('limit'))+1,'registros_por_pagina' => $request->get('limit'), 'apenas_importado_api' => 'N'];
        $oClienteOmie = new ClientesCadastroJsonClient();
        $response = $oClienteOmie->ListarClientes($payload);
        $aRetorno['total'] = $response->total_de_registros;

        foreach($response->clientes_cadastro as $oCliente){
            $oDados = new \stdClass();
            $oDados->id              = $oCliente->codigo_cliente_omie;
            $oDados->name            = $oCliente->nome_fantasia;
            $oDados->email           = isset($oCliente->email) ? $oCliente->email : '';
            $aRetorno['rows'][] = $oDados;
        }

        return json_encode($aRetorno);
    }
}
