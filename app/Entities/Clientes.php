<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Illuminate\Http\Request;
/**
 * @ORM\Entity
 * @ORM\Table(name="clientes")
 */
class Clientes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $numero_telefone;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $tipo_cliente;

    public function __construct(Request $request)
    {
        $this->setName($request->get('name'));
        $this->setEmail($request->get('email'));
        $this->setTipo_cliente($request->get('tipo_cliente'));
        $this->setNumero_telefone($request->get('numero_telefone'));
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getNumero_telefone(){
        return $this->numero_telefone;
    }

    public function setNumero_telefone($numero_telefone){
        $this->numero_telefone = $numero_telefone;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getTipo_cliente(){
        return $this->tipo_cliente;
    }

    public function setTipo_cliente($tipo_cliente){
        $this->tipo_cliente = $tipo_cliente;
    }

}