<?php

namespace App\Repository;
use App\Entities\Clientes;
use Doctrine\ORM\EntityManager;
use Illuminate\Http\Request;

class ClientesRepo
{

    /**
     * @var string
     */
    private $class = 'App\Entities\Clientes';
    /**
     * @var EntityManager
     */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function create(Clientes $oCliente)
    {
        $this->em->persist($oCliente);
        $this->em->flush();
    }

    public function update(Clientes $oCliente, Request $request)
    {
        $oCliente->setName($request->get('name'));
        $oCliente->setEmail($request->get('email'));
        $oCliente->setTipo_cliente($request->get('tipo_cliente'));
        $oCliente->setNumero_telefone($request->get('numero_telefone'));
        $this->em->persist($oCliente);
        $this->em->flush();
    }

    /**
     * @return Clientes
     */
    public function CleinteById($id)
    {
        return $this->em->getRepository($this->class)->findOneBy([
            'id' => $id
        ]);
    }

    public function delete(Clientes $oCliente)
    {
        $this->em->remove($oCliente);
        $this->em->flush();
    }

    /**
     * create Clientes
     * @return Clientes
     */
    public function prepareData(Request $request)
    {
        return new Clientes($request);
    }
}