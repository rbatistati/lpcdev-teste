$(document).ready(function() {


    // BOOTSTRAP TABLE - CUSTOM TOOLBAR
    // =================================================================
    // Require Bootstrap Table
    // http://bootstrap-table.wenzhixin.net.cn/
    // =================================================================
    var $table = $('#tblFormas'),	$remove = $('#apagarForma'), $editar = $('#editarForma');
    $table.bootstrapTable('showLoading');
    $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
    });

    $remove.click(function () {
        var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id;
        });

        if(ids.length == 0){
            $.notify('Selecione pelo menos um registro para ser excluído.','info');
        }else {

            swal({
                title: "Você tem certeza?",
                text: "Todas os registros selecionados serão apagadas!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não"
            }).then(function () {
                $.ajax({
                    type: 'post',
                    url: 'delete',
                    data: {dados: ids, _token: $('meta[name="csrf-token"]').attr('content')},
                    dataType: 'json'
                }).always(function (data) {
                    if(data.class == 'success'){
                        $table.bootstrapTable('remove', {
                            field: 'id',
                            values: ids
                        });
                    }
                    $table.bootstrapTable('refresh');
                    swal("", data.msg, data.class);
                });
            });

        }

    });

    $table.on('click-row.bs.table', function(e, row, $element){
        var id = [row.id];
        $('#modal').load('edit',{id: id,_token: $('meta[name="csrf-token"]').attr('content')}, function() {
            $('#modalAddForma').modal();
        });
    });


    $editar.click(function () {

        var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id;
        });
        var index = [];
        $('input[name="btSelectItem"]:checked').each(function () {
            index.push($(this).data('index'));
        });

        if(ids.length == 0){
            $.notify("Selecione um registro para ser alterado", "info");
        }else if(ids.length > 1) {
            $.notify('Só é permitido alterar um registro por vez.','info');
        } else {
            $('#modal').load('edit',{id: ids,_token: $('meta[name="csrf-token"]').attr('content')}, function() {
                $('#modalAddForma').modal();
            });
        }

    });
});

// FORMAT COLUMN
// Use "data-formatter" on HTML to format the display of bootstrap table column.
// =================================================================


// Sample format for Invoice Column.
// =================================================================
function invoiceFormatter(value, row) {
    return '<a href="#" class="btn-link" > Order #' + value + '</a>';
}

// Sample Format for User Name Column.
// =================================================================
function nameFormatter(value, row) {
    return '<a href="#" class="btn-link" > ' + value + '</a>';
}

// Sample Format for Order Date Column.
// =================================================================
function dateFormatter(value, row) {
    var icon = row.id % 2 === 0 ? 'fa-star' : 'fa-user';
    return '<span class="text-muted"> ' + value + '</span>';
}


// Sample Format for Order Status Column.
// =================================================================
function statusFormatter(value, row) {
    var labelColor;
    if (value == "Paid") {
        labelColor = "success";
    }else if(value == "Unpaid"){
        labelColor = "warning";
    }else if(value == "Shipped"){
        labelColor = "info";
    }else if(value == "Refunded"){
        labelColor = "danger";
    }
    var icon = row.id % 2 === 0 ? 'fa-star' : 'fa-user';
    return '<div class="label label-table label-'+ labelColor+'"> ' + value + '</div>';
}


// Sort Price Column
// =================================================================
function priceSorter(a, b) {
    a = +a.substring(1); // remove $
    b = +b.substring(1);
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
}

